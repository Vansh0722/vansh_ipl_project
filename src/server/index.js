const csvtojson = require("csvtojson");
const matchesPlayedPerYear = require("./matchesPlayedPerYears");
const fs = require("fs");
const csvfilepath = "../data/matches.csv";
const JSON_OUTPUT = "../data/matchesPlayedPerYear.json"

function main() {
  csvtojson()
    .fromFile(csvfilepath)
    .then((matches) => {
      let result = matchesPlayedPerYear(matches)
      resultMatchesPlayedPerYear(result)
      // console.log(matchesPlayedPerYear(matches))
      // console.log(matches.slice(0, 6));
    });
}
function resultMatchesPlayedPerYear (result){
  const json ={
    matchesPlayedPerYear:result
  }
  const jString = JSON.stringify(json)
  fs.writeFile(JSON_OUTPUT, jString, "utf8", err =>{
    if(err){
      console.log(err)
    }
  })
}
main();
