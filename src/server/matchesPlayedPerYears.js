function matchesPlayedPerYear(played) {
  const perYear = played.reduce((matchesPlayed, val) => {
    if (matchesPlayed[val.season]) {
      matchesPlayed[val.season] += 1;
    } else {
      matchesPlayed[val.season] = 1;
    }
    return matchesPlayed;
  }, {});

  return perYear;
}

module.exports = matchesPlayedPerYear;
